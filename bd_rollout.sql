begin;

--drop sequence main_id_sequence;
--drop function getnextid;

create sequence main_id_sequence;

create function getnextid() returns char(8) as
    $$ declare
      str text :=  '0123456789abcdefghijklmnopqrstuvwxyz';
      val bigint;
      id_ text;
      mod int;
      begin
      val:=nextval('main_id_sequence');
      id_:='';
      while (length(id_) < 8) loop
        mod = val % 36;
        id_:=substring(str,mod+1,1)||id_;
        val = val / 36;
      end loop;
      return id_;
      return 'null';
      end;   $$
language plpgsql;

insert into _roles values (getnextid(), 'admin', 'админ', 999);
insert into _roles values (getnextid(), 'boss', 'Руководитель', 2);
insert into _roles values (getnextid(), 'dispatcher', 'Диспетчер', 1);
insert into _roles values (getnextid(), 'driver', 'Водитель', 0);

insert into _vehicles values (getnextid(), '№1 – 30', 100, 30);
insert into _vehicles values (getnextid(), '№2 – 10', 50, 30);

commit;