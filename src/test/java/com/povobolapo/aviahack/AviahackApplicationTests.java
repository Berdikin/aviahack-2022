package com.povobolapo.aviahack;

import com.povobolapo.aviahack.dispatcher.Event;
import com.povobolapo.aviahack.dispatcher.EventDispatcher;
import com.povobolapo.aviahack.dispatcher.EventDispatcherImpl;
import com.povobolapo.aviahack.dispatcher.EventHandler;
import com.povobolapo.aviahack.dto.TaskPerformerRequest;
import com.povobolapo.aviahack.dto.VehicleDriverDto;
import com.povobolapo.aviahack.dto.task.TaskDto;
import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.model.VehicleEntity;
import com.povobolapo.aviahack.repository.VehicleRepository;
import com.povobolapo.aviahack.service.TaskPerformerService;
import com.povobolapo.aviahack.service.TaskService;
import com.povobolapo.aviahack.service.VehicleDriverService;
import com.povobolapo.aviahack.service.impl.UserDetailsServiceImpl;
import com.povobolapo.aviahack.utils.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.naming.AuthenticationException;
import javax.transaction.Transactional;

@SpringBootTest(classes = AviahackApplication.class)
class AviahackApplicationTests {
	private static final String AUTOTEST_LOGIN = "autotest_user";

	private JwtTokenUtil jwtTokenUtil;
	private VehicleDriverService vehicleDriverService;
	private VehicleRepository vehicleRepository;
	private TaskService taskService;
	private TaskPerformerService taskPerformerService;
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	public AviahackApplicationTests(JwtTokenUtil jwtTokenUtil, VehicleDriverService vehicleDriverService, VehicleRepository vehicleRepository, TaskService taskService, TaskPerformerService taskPerformerService, UserDetailsServiceImpl userDetailsService) {
		this.jwtTokenUtil = jwtTokenUtil;
		this.vehicleDriverService = vehicleDriverService;
		this.vehicleRepository = vehicleRepository;
		this.taskService = taskService;
		this.taskPerformerService = taskPerformerService;
		this.userDetailsService = userDetailsService;
	}

	@Test
	public void authTest() {
		String token = jwtTokenUtil.generateToken(AUTOTEST_LOGIN);
		assert StringUtils.isNotBlank(token);
		assert null != jwtTokenUtil.validateToken(token);
	}

	@Test
	public void testTasks() throws AuthenticationException {
		setSecurityContext(AUTOTEST_LOGIN);
		taskService.getTasks();
		setSecurityContext("user_4");
		taskService.getTasks();
	}

	@Test
	@Transactional
	public void addDrivers() {
		VehicleEntity vehicle = vehicleRepository.findAll().get(1);
		VehicleDriverDto vehicleDriverDto = vehicleDriverService.createVehicleDriver(AUTOTEST_LOGIN, vehicle.getId());
		assert vehicle.getId().equals(vehicleDriverDto.getVehicle().getId());
		assert AUTOTEST_LOGIN.equals(vehicleDriverDto.getDriver().getLogin());
	}

	@Test
	void createTaskAndAddPerformers() throws Exception {
		setSecurityContext(AUTOTEST_LOGIN);
		TaskDto taskDto = taskService.getById("0000005q");
		VehicleDriverDto vehicleDriverDto = vehicleDriverService.getAllDrivers().get(0);
		TaskPerformerDto taskPerformerDto = taskPerformerService.addTaskPerformer(new TaskPerformerRequest(taskDto.getTaskId(), vehicleDriverDto.getId()));
		assert taskPerformerDto.getTaskStatus().equals("Создано");
		assert taskPerformerDto.getDriverId().equals(vehicleDriverDto.getId());

		TaskDto fullTask = taskService.getById(taskDto.getTaskId());
		assert fullTask.getTaskPerformerList().stream().anyMatch(p -> p.getTaskPerformerId().equals(taskPerformerDto.getTaskPerformerId()));

		taskPerformerService.deleteTaskPerformer(new TaskPerformerRequest(taskDto.getTaskId(), vehicleDriverDto.getId()));
		fullTask = taskService.getById(taskDto.getTaskId());
		assert fullTask.getTaskPerformerList().stream().noneMatch(p -> p.getTaskPerformerId().equals(taskPerformerDto.getTaskPerformerId()));
	}

	@Test
	void testEventDipatcher() {
		EventDispatcher eventDispatcher = new EventDispatcherImpl();
		DummyHandler h = new DummyHandler();
		eventDispatcher.registerHandler(DummyEvent.class, (EventHandler<DummyEvent>) h::onEvent);
		try {
			eventDispatcher.dispatch(new DummyEvent());
		} catch (Exception ex) {
			assert ((RuntimeException) ex).getMessage().equals("yeah");
		}
	}

	private class DummyEvent implements Event {
	}
	private class DummyHandler implements EventHandler<DummyEvent>{
		@Override
		public void onEvent(DummyEvent event) throws Exception {
			throw new RuntimeException("yeah");
		}
	}

	private void setSecurityContext(String login) {
		UserDetails userDetails = userDetailsService.loadUserByUsername(login);
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				userDetails, null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
	}
}
