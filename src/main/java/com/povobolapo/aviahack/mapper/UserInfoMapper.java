package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.model.UserInfoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface UserInfoMapper {
    UserInfoEntity toEntity(UserInfoDto userInfoDto);
    UserInfoDto toDto(UserInfoEntity userInfoEntity);
}
