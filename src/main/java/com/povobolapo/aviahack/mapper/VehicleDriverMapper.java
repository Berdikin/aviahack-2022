package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.VehicleDriverDto;
import com.povobolapo.aviahack.model.VehicleDriverEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VehicleDriverMapper {
    VehicleDriverDto toDto(VehicleDriverEntity vehicleDriverEntity);
}
