package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.task.TaskDto;
import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.dto.task.TaskShortDto;
import com.povobolapo.aviahack.model.FlightEntity;
import com.povobolapo.aviahack.model.RoadEntity;
import com.povobolapo.aviahack.model.TaskEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TaskPerformerMapper.class})
public interface TaskMapper {
    @Mappings({
            @Mapping(target = "taskId", source = "id"),
            @Mapping(target = "flightNumber", source = "flight.flightNumber"),
    })
    TaskShortDto toShortDto(TaskEntity taskEntity);
    List<TaskShortDto> toListShortDto(List<TaskEntity> taskEntity);

    @Mappings({
            @Mapping(target = "taskId", source = "id"),
            @Mapping(target = "roadId", source = "road.id"),
            @Mapping(target = "flightNumber", source = "flight.flightNumber"),
            @Mapping(target = "sourcePoint", source = "road.sourcePoint.pointName"),
            @Mapping(target = "targetPoint", source = "road.targetPoint.pointName"),
            @Mapping(target = "distance", source = "road.distance"),
            @Mapping(target = "taskPerformerList", source = "taskPerformers"),
    })
    TaskDto toDto(TaskEntity taskEntity);

    @Mappings({
            @Mapping(target = "id", source = "taskDto.taskId"),
            @Mapping(target = "creationDate", source = "taskDto.creationDate"),
            @Mapping(target = "startDate", source = "taskDto.startDate"),
            @Mapping(target = "estimationTime", source = "taskDto.estimationTime"),
            @Mapping(target = "flight", source = "flight"),
            @Mapping(target = "road", source = "road"),
    })
    TaskEntity toEntity(TaskDto taskDto, FlightEntity flight, RoadEntity road);
}
