package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.model.TaskPerformerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskPerformerMapper {
    @Mappings({
            @Mapping(target = "taskPerformerId", source = "id"),
            @Mapping(target = "taskStatus", source = "status.caption"),
            @Mapping(target = "driverName", source = "vehicleDriver.driver.name"),
            @Mapping(target = "driverId", source = "vehicleDriver.id"),
            @Mapping(target = "vehicle", source = "vehicleDriver.vehicle"),
    })
    TaskPerformerDto toShortDto(TaskPerformerEntity taskPerformerEntity);

    List<TaskPerformerDto> toListShortDto(List<TaskPerformerEntity> taskPerformerEntity);
}
