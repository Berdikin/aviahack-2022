package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.task.TaskStatusEntityDto;
import com.povobolapo.aviahack.model.TaskStatusEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskStatusMapper {
    TaskStatusEntity toEntity(TaskStatusEntityDto dto);
    TaskStatusEntityDto toDto(TaskStatusEntity entity);
}
