package com.povobolapo.aviahack.mapper;

import com.povobolapo.aviahack.dto.VehicleDto;
import com.povobolapo.aviahack.model.VehicleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface VehicleMapper {

    VehicleDto toDto(VehicleEntity vehicle);

}
