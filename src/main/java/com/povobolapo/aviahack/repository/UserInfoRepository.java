package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.UserInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfoEntity, String> {
    UserInfoEntity findByLogin(String login);
    List<UserInfoEntity> findByRoleName(String roleName);
}
