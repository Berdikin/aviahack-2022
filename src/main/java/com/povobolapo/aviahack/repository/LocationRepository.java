package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<LocationEntity, String> {
    LocationEntity findByLocationName(String locationName);
}
