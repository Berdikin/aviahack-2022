package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.TaskEntity;
import com.povobolapo.aviahack.model.TaskPerformerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface TaskRepository extends JpaRepository<TaskEntity, String> {
    @Query("select count(t) from TaskPerformerEntity t where t.id=:taskPerformerId and t.status.name = 'finished'")
    Integer countByTaskPerformersCompleted(String taskPerformerId);

    @Query("select count(t) from TaskPerformerEntity t where t.id=:taskPerformerId and t.status.id = (select ts.id from TaskStatusEntity ts where ts.name= 'created')")
    Integer countByTaskPerformersRejected(String taskPerformerId);

    @Query("select count(t) from TaskPerformerEntity t where t.id=:taskPerformerId and t.status.name = 'in_progress'")
    Integer countByTaskPerformersAccepted(String taskPerformerId);

    //метод для подсчета опоздания по задаче

    List<TaskEntity> findByTaskPerformersIn(List<TaskPerformerEntity> taskPerformerEntities);
}