package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.LocationEntity;
import com.povobolapo.aviahack.model.PointLocationsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointsLocationsRepository extends JpaRepository<PointLocationsEntity, String> {
    PointLocationsEntity findByLocation(LocationEntity locationEntity);
}
