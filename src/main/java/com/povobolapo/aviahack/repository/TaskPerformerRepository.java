package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.TaskPerformerEntity;
import com.povobolapo.aviahack.model.VehicleDriverEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskPerformerRepository extends JpaRepository<TaskPerformerEntity, String> {
    List<TaskPerformerEntity> findAllByTaskId(String id);
    TaskPerformerEntity findByTaskIdAndVehicleDriverId(String taskId, String vehicleDriver);
    List<TaskPerformerEntity> findByVehicleDriver(VehicleDriverEntity vehicleDriverEntity);

    @Query("select tp from TaskPerformerEntity tp, TaskEntity task where tp.status.name = 'finished' and tp.taskId = task.id and task.finishDate =" +
            "(select max(t.finishDate) from TaskEntity t where tp member of t.taskPerformers)")
    List<TaskPerformerEntity> findAllTaskPerformers();
}
