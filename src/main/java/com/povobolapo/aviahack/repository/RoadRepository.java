package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.PointEntity;
import com.povobolapo.aviahack.model.RoadEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoadRepository extends JpaRepository<RoadEntity, String> {
    RoadEntity findBySourcePointAndTargetPoint(PointEntity pointSourceId, PointEntity pointTargetId);
    Integer getDistanceBySourcePointAndTargetPoint(PointEntity pointSourceId, PointEntity pointTargetId);

}