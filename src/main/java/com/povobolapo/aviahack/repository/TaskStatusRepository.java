package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.TaskStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskStatusRepository extends JpaRepository<TaskStatusEntity, String> {
    TaskStatusEntity findByName(String name);
}
