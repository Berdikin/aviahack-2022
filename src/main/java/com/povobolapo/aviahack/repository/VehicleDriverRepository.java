package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.UserInfoEntity;
import com.povobolapo.aviahack.model.VehicleDriverEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleDriverRepository extends JpaRepository<VehicleDriverEntity, String> {
    List<VehicleDriverEntity> findAll();
    VehicleDriverEntity findByDriverId(String driverId);
    VehicleDriverEntity findByDriver(UserInfoEntity userInfoEntity);
}
