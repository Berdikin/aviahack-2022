package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<VehicleEntity, String> {
}
