package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.FlightEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepository extends JpaRepository<FlightEntity, String> {
    FlightEntity findByFlightNumber(String flightNumber);
}