package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.PointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<PointEntity, String> {
    PointEntity findByPointName(String pointName);
}
