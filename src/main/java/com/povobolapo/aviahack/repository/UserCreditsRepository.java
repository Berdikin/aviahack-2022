package com.povobolapo.aviahack.repository;

import com.povobolapo.aviahack.model.UserCreditsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCreditsRepository extends JpaRepository<UserCreditsEntity, String> {
    UserCreditsEntity findByLogin(String login);
}
