package com.povobolapo.aviahack.service;

import com.povobolapo.aviahack.dto.UserCreateDto;
import com.povobolapo.aviahack.dto.user.UserInfoDto;

import javax.naming.AuthenticationException;
import java.nio.file.AccessDeniedException;

public interface UserService {
    UserInfoDto createUser(UserCreateDto userCreateDto);
    UserInfoDto getUser(String login);
    void updateUser(UserInfoDto userInfoDto) throws AuthenticationException, AccessDeniedException;
    void deleteUser(String login) throws AccessDeniedException, AuthenticationException;
    UserInfoDto getCurrentUser() throws AuthenticationException;
    String authenticatedUserName() throws AuthenticationException;
}
