package com.povobolapo.aviahack.service.impl;

import com.povobolapo.aviahack.controller.exception.NotFoundException;
import com.povobolapo.aviahack.dispatcher.EventDispatcher;
import com.povobolapo.aviahack.dto.TaskPerformerRequest;
import com.povobolapo.aviahack.dto.task.PerformerTaskActionDto;
import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.mapper.TaskPerformerMapper;
import com.povobolapo.aviahack.model.TaskEntity;
import com.povobolapo.aviahack.model.TaskPerformerEntity;
import com.povobolapo.aviahack.model.TaskStatusEntity;
import com.povobolapo.aviahack.model.VehicleDriverEntity;
import com.povobolapo.aviahack.repository.TaskPerformerRepository;
import com.povobolapo.aviahack.repository.TaskRepository;
import com.povobolapo.aviahack.repository.TaskStatusRepository;
import com.povobolapo.aviahack.repository.VehicleDriverRepository;
import com.povobolapo.aviahack.service.TaskPerformerService;
import com.povobolapo.aviahack.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.AuthenticationException;
import java.nio.file.AccessDeniedException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Scope("singleton")
public class TaskPerformerServiceImpl implements TaskPerformerService {
    private static final Logger log = LoggerFactory.getLogger(TaskPerformerServiceImpl.class);

    private final TaskPerformerRepository taskPerformerRepository;
    private final TaskStatusRepository taskStatusRepository;
    private final TaskRepository taskRepository;
    private final VehicleDriverRepository vehicleDriverRepository;

    private final TaskPerformerMapper taskPerformerMapper;

    private final UserService userService;
    private final EventDispatcher eventDispatcher;

    private Map<String, String> statusBySignal = Map.of(
            "accept", "idle",
            "start", "in_progress",
            "complete", "finished"
    );
    // Жесть
    private Map<String, String> signalByStatus = Map.of(
            "created", "accept",
            "idle", "accept",
            "in_progress", "start",
            "finished", "complete"
    );
    // Большая ошибка. Надо вынести в бд
    private Map<String, String> signalCaption = Map.of(
            "accept", "Принять",
            "start", "Начать",
            "complete", "Завершить"
    );

    @Override
    public PerformerTaskActionDto getTaskActionByTaskId(String taskId) throws AuthenticationException {
        Optional<TaskEntity> taskEntity = taskRepository.findById(taskId);
        if (taskEntity.isEmpty()) {
            throw new NotFoundException("Can't find task by id " + taskId);
        }
        return getTaskAction(taskEntity.get());
    }

    @Override
    public PerformerTaskActionDto getTaskAction(TaskEntity taskEntity) throws AuthenticationException {
        UserInfoDto userInfo = userService.getCurrentUser();
        VehicleDriverEntity vehicleDriverEntity = vehicleDriverRepository.findByDriverId(userInfo.getId());
        if (vehicleDriverEntity == null) {
            throw new NotFoundException("Current user is not driver!");
        }
        TaskPerformerEntity taskPerformer = taskPerformerRepository.findByTaskIdAndVehicleDriverId(taskEntity.getId(), vehicleDriverEntity.getId());
        if (taskPerformer == null) {
            throw new NotFoundException("Current user is not performer in task " + taskEntity.getId());
        }
        return getTaskAction(taskPerformer);
    }

    @Override
    public PerformerTaskActionDto getTaskAction(TaskPerformerEntity taskPerformer) {
        String signal = signalByStatus.get(taskPerformer.getStatus().getName());
        if (signal == null) {
            return new PerformerTaskActionDto();
        }

        return new PerformerTaskActionDto(signal, signalCaption.get(signal));
    }

    @Transactional
    public void changeTaskStatus(PerformerTaskActionDto taskActionDto) {
        final Optional<TaskEntity> byId = taskRepository.findById(taskActionDto.getTaskId());
        if (byId.isEmpty()) {
            throw new NotFoundException("Can't find task by id " + taskActionDto.getTaskId());
        }

        Optional<TaskPerformerEntity> performer = taskPerformerRepository.findById(taskActionDto.getPerformerId());
        if (performer.isEmpty()) {
            throw new NotFoundException("Can't find taskPerfromer by id" + taskActionDto.getPerformerId());
        }

        String status = statusBySignal.get(taskActionDto.getSignal());
        Objects.requireNonNull(status, "Wrong action signal! Valid signals: accept, start, complete");
        final TaskStatusEntity byName = taskStatusRepository.findByName(status);
        Objects.requireNonNull(byName);

        performer.get().setStatus(byName);
        taskPerformerRepository.save(performer.get());
        // add event dispatcher
    }

    // TODO добавить dispatchEvent
    @Override
    public TaskPerformerDto addTaskPerformer(TaskPerformerRequest performerRequest) throws AuthenticationException, AccessDeniedException {
        if (cantEditTask()) {
            throw new AccessDeniedException("Current user can't edit tasks");
        }

        TaskPerformerEntity taskPerformer = taskPerformerRepository.findByTaskIdAndVehicleDriverId(
                performerRequest.getTaskId(), performerRequest.getDriverId());
        if (taskPerformer != null) {
            log.info("Task performer already exist");
            return taskPerformerMapper.toShortDto(taskPerformer);
        }

        Optional<VehicleDriverEntity> vehicleDriver = vehicleDriverRepository.findById(performerRequest.getDriverId());
        if (vehicleDriver.isEmpty()) {
            throw new NotFoundException("Can't find vehicle drive by id " + performerRequest.getDriverId());
        }

        Optional<TaskEntity> task = taskRepository.findById(performerRequest.getTaskId());
        if (task.isEmpty()) {
            throw new NotFoundException("Can't find task by id " + performerRequest.getTaskId());
        }

        TaskStatusEntity taskStatusEntity = taskStatusRepository.findByName("created");
        Objects.requireNonNull(taskStatusEntity);

        TaskPerformerEntity taskPerformerEntity = new TaskPerformerEntity();
        taskPerformerEntity.setStatus(taskStatusEntity);
        taskPerformerEntity.setTaskId(task.get().getId());
        taskPerformerEntity.setVehicleDriver(vehicleDriver.get());
        taskPerformerRepository.save(taskPerformerEntity);

        log.info("Added new task performer {} in task {}", taskPerformerEntity.getId(), task.get().getId());
        return taskPerformerMapper.toShortDto(taskPerformerEntity);
    }

    @Override
    public void deleteTaskPerformer(TaskPerformerRequest performerRequest) throws AuthenticationException, AccessDeniedException {
        if (cantEditTask()) {
            throw new AccessDeniedException("Current user can't edit tasks");
        }

        TaskPerformerEntity taskPerformer = taskPerformerRepository.findByTaskIdAndVehicleDriverId(
                performerRequest.getTaskId(), performerRequest.getDriverId());

        if (taskPerformer == null) {
            throw new NotFoundException("Can't find performer by data " + performerRequest);
        }

        taskPerformerRepository.delete(taskPerformer);
        log.info("Deleted task performer {} in task {}", taskPerformer.getId(), taskPerformer.getTaskId());
    }

    private boolean cantEditTask() throws AuthenticationException {
        UserInfoDto user = userService.getCurrentUser();
        return user.getRole().getPriority() <= 0;
    }
}
