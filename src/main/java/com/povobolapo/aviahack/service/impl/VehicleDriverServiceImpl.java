package com.povobolapo.aviahack.service.impl;

import com.povobolapo.aviahack.controller.exception.NotFoundException;
import com.povobolapo.aviahack.dto.VehicleDriverDto;
import com.povobolapo.aviahack.mapper.VehicleDriverMapper;
import com.povobolapo.aviahack.model.UserInfoEntity;
import com.povobolapo.aviahack.model.VehicleDriverEntity;
import com.povobolapo.aviahack.model.VehicleEntity;
import com.povobolapo.aviahack.repository.UserInfoRepository;
import com.povobolapo.aviahack.repository.VehicleDriverRepository;
import com.povobolapo.aviahack.repository.VehicleRepository;
import com.povobolapo.aviahack.service.VehicleDriverService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Scope("singleton")
public class VehicleDriverServiceImpl implements VehicleDriverService {
    private final VehicleDriverRepository vehicleDriverRepository;
    private final VehicleRepository vehicleRepository;
    private final VehicleDriverMapper vehicleDriverMapper;
    private final UserInfoRepository userInfoRepository;

    @Override
    public VehicleDriverDto createVehicleDriver(String login, String vehicleId) {
        UserInfoEntity userInfoEntity = userInfoRepository.findByLogin(login);
        if (userInfoEntity == null) {
            throw new NotFoundException("Can't find user with login " + login);
        }

        Optional<VehicleEntity> vehicleOptional = vehicleRepository.findById(vehicleId);
        if (vehicleOptional.isEmpty()) {
            throw new NotFoundException("Can't found vehicle with id " + vehicleId);
        }

        VehicleDriverEntity driverEntity = new VehicleDriverEntity();
        driverEntity.setDriver(userInfoEntity);
        driverEntity.setVehicle(vehicleOptional.get());
        vehicleDriverRepository.save(driverEntity);
        return vehicleDriverMapper.toDto(driverEntity);
    }

    @Override
    public List<VehicleDriverDto> getAllDrivers() {
        List<VehicleDriverEntity> vehicleDriverEntities = vehicleDriverRepository.findAll();
        if (vehicleDriverEntities == null) {
            return List.of();
        }
        return vehicleDriverEntities.stream().map(vehicleDriverMapper::toDto).collect(Collectors.toList());
    }
}
