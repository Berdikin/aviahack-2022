package com.povobolapo.aviahack.service.impl;

import com.povobolapo.aviahack.controller.exception.NotFoundException;
import com.povobolapo.aviahack.dto.UserCreateDto;
import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.mapper.UserInfoMapper;
import com.povobolapo.aviahack.model.UserCreditsEntity;
import com.povobolapo.aviahack.model.UserInfoEntity;
import com.povobolapo.aviahack.repository.UserCreditsRepository;
import com.povobolapo.aviahack.repository.UserInfoRepository;
import com.povobolapo.aviahack.service.UserService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.validation.constraints.NotEmpty;
import java.nio.file.AccessDeniedException;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserInfoRepository userInfoRepository;
    private final UserCreditsRepository userCreditsRepository;
    private final UserInfoMapper userInfoMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserInfoDto createUser(UserCreateDto userCreateDto) {
        UserInfoEntity userInfoEntity = userInfoRepository.findByLogin(userCreateDto.getLogin());
        if (userInfoEntity != null) {
            return userInfoMapper.toDto(userInfoEntity);
        }

        UserCreditsEntity userCreditsEntity = new UserCreditsEntity();
        userCreditsEntity.setLogin(userCreateDto.getLogin());
        userCreditsEntity.setPassword(passwordEncoder.encode(userCreateDto.getPassword()));
        userCreditsRepository.save(userCreditsEntity);

        userInfoEntity = userInfoMapper.toEntity(userCreateDto);
        return userInfoMapper.toDto(userInfoRepository.save(userInfoEntity));
    }

    @Override
    public UserInfoDto getUser(String login) {
        UserInfoEntity userInfoEntity = userInfoRepository.findByLogin(login);
        if (userInfoEntity == null) {
            throw new NotFoundException("Can't find user with login " + login);
        }
        return userInfoMapper.toDto(userInfoEntity);
    }

    @Override
    public void updateUser(UserInfoDto userInfoDto) throws AuthenticationException, AccessDeniedException {
        UserInfoEntity userToUpdate = userInfoRepository.findByLogin(userInfoDto.getLogin());
        if (userToUpdate == null) {
            throw new NotFoundException("Can't find user with login " + userInfoDto.getLogin());
        }
        if (!canEditUser(userToUpdate)) {
            throw new AccessDeniedException("Current user can't edit user " + userToUpdate.getLogin());
        }
        userToUpdate.setActive(userInfoDto.isActive());
        userToUpdate.setName(userInfoDto.getName());
        userInfoRepository.save(userToUpdate);
    }

    @Override
    public void deleteUser(String login) throws AccessDeniedException, AuthenticationException {
        UserInfoEntity userToDelete = userInfoRepository.findByLogin(login);
        if (userToDelete == null) {
            throw new NotFoundException("Can't find user with login " + login);
        }
        if (canEditUser(userToDelete)) {
            throw new AccessDeniedException("Current user can't edit user " + login);
        }
        userInfoRepository.delete(userToDelete);
    }

    @Override
    public UserInfoDto getCurrentUser() throws AuthenticationException {
        return getUser(authenticatedUserName());
    }

    /***
     * Метод для получения логина текущего пользователя. Если текущая сессия каким-то образом не авторизована - вылетит ошибка.
     * Гарантируется. что метод вернет не пустую строку.
     * @return String логин текущего пользователя
     * @throws AuthenticationException если сессия не авторизована
     */
    @NonNull
    @NotEmpty
    public String authenticatedUserName() throws AuthenticationException {
        // Получем из контекста безопасности какой юзер сейчас делает запрос
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null || StringUtils.isBlank(currentUser.getName())) {
            throw new AuthenticationException("Current user didn't authenticate!");
        }
        return currentUser.getName();
    }

    // Юзера может редактировать либо он сам, либо юзер с большим приоритетом по роли
    private boolean canEditUser(UserInfoEntity userToEdit) throws AuthenticationException {
        String currentLogin = authenticatedUserName();
        if (currentLogin.equals(userToEdit.getLogin())) {
            return true;
        }
        UserInfoEntity currentUser = userInfoRepository.findByLogin(currentLogin);
        return userToEdit.getRole().getPriority() < currentUser.getRole().getPriority();
    }
}
