package com.povobolapo.aviahack.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.povobolapo.aviahack.controller.exception.NotFoundException;
import com.povobolapo.aviahack.dispatcher.EventDispatcher;
import com.povobolapo.aviahack.dto.optimization.OptimizationRequest;
import com.povobolapo.aviahack.dto.optimization.OptimizationVehicleDriverDto;
import com.povobolapo.aviahack.dto.task.TaskDto;
import com.povobolapo.aviahack.dto.task.TaskShortDto;
import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.mapper.TaskMapper;
import com.povobolapo.aviahack.model.*;
import com.povobolapo.aviahack.repository.*;
import com.povobolapo.aviahack.service.TaskService;
import com.povobolapo.aviahack.service.UserService;
import com.povobolapo.aviahack.websocket.model.WsEvent;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);
    private static final int MIN_PRIORITY_TO_EDIT = 1;

    private final TaskRepository taskRepository;
    private final FlightRepository flightRepository;
    private final RoadRepository roadRepository;
    private final TaskPerformerRepository taskPerformerRepository;
    private final UserInfoRepository userInfoRepository;
    private final VehicleDriverRepository vehicleDriverRepository;
    private final PointsLocationsRepository pointsLocationsRepository;

    private final TaskMapper taskMapper;
    private final EventDispatcher eventDispatcher;
    private final UserService userService;

    @Override
    public List<TaskShortDto> getTasks() throws AuthenticationException {
        UserInfoEntity currentUser = userInfoRepository.findByLogin(userService.authenticatedUserName());
        if (currentUser.getRole().getPriority() > 0) {
            return taskMapper.toListShortDto(taskRepository.findAll());
        }
        return taskMapper.toListShortDto(getUserTask(currentUser));
    }

    private List<TaskEntity> getUserTask(UserInfoEntity userInfoEntity) {
        VehicleDriverEntity vehicleDriver = vehicleDriverRepository.findByDriver(userInfoEntity);
        if (vehicleDriver == null) {
            return List.of();
        }

        List<TaskPerformerEntity> taskPerformerEntities = taskPerformerRepository.findByVehicleDriver(vehicleDriver);
        if (taskPerformerEntities == null || taskPerformerEntities.isEmpty()) {
            return List.of();
        }
        return taskRepository.findByTaskPerformersIn(taskPerformerEntities);
    }

    @Override
    public TaskDto getById(String taskId) {
        return taskMapper.toDto(taskRepository.findById(taskId)
                .orElseThrow(() -> new NotFoundException("Не удалось найти задачу по id=" + taskId)));
    }

    @Override
    @Transactional
    public TaskDto createTask(String flightNumber) throws AccessDeniedException, AuthenticationException {
        if (!canEdit()) {
            throw new AccessDeniedException("Current user can't edit tasks");
        }

        FlightEntity flight = flightRepository.findByFlightNumber(flightNumber);
        if (flight == null) {
            throw new RuntimeException("Can't find flight by flight number " + flightNumber);
        }

        PointLocationsEntity gatePoint = pointsLocationsRepository.findByLocation(flight.getGateLocation());
        PointLocationsEntity parkPoint = pointsLocationsRepository.findByLocation(flight.getParkLocation());
        Objects.requireNonNull(gatePoint);
        Objects.requireNonNull(parkPoint);

        RoadEntity roadEntity;
        switch (flight.getFlightType()) {
            case "D":
                roadEntity = roadRepository.findBySourcePointAndTargetPoint(gatePoint.getPoint(), parkPoint.getPoint());
                break;
            case "A":
                roadEntity = roadRepository.findBySourcePointAndTargetPoint(parkPoint.getPoint(), gatePoint.getPoint());
                break;
            default:
                throw new RuntimeException("Can't determine flight type");
        }
        Objects.requireNonNull(roadEntity);

        TaskEntity task = new TaskEntity();
        task.setCreationDate(LocalDateTime.now());
        task.setRoad(roadEntity);
        task.setFlight(flight);
        task.setIsDeleted(false);
        taskRepository.save(task);

        log.info("New task for flight {} was created id  {}", flightNumber, task.getId());
        dispatch(task);
        return taskMapper.toDto(task);
    }

    @Override
    @Transactional
    public void updateTask(TaskDto taskDto) throws AuthenticationException, AccessDeniedException {
        if (!canEdit()) {
            throw new AccessDeniedException("Current user can't edit tasks");
        }

        Optional<TaskEntity> task = taskRepository.findById(taskDto.getTaskId());
        if (task.isEmpty()) {
            throw new NotFoundException("Can't find task with id " + taskDto.getTaskId());
        }
        TaskEntity taskEntity = task.get();
        taskEntity.setStartDate(taskDto.getStartDate());
        taskEntity.setEstimationTime(taskDto.getEstimationTime());
        taskRepository.save(taskEntity);
        log.info("Task {} was updated", taskEntity.getId());
        dispatch(taskEntity);
    }

    // TODO ДАННЫЙ МЕТОД НЕ ЗАВЕРШЕН И НЕ ПРОТЕСТИРОВАН
    // Не поддерживается создание задания и исполнителей
    // Необходимо добавить заполнение примерного времени исполнения

    @Override
    @Transactional
    public List<TaskPerformerEntity> generateTaskPerformers(String taskId) throws IOException, InterruptedException {
        Optional<TaskEntity> taskOptional = taskRepository.findById(taskId);
        if (taskOptional.isEmpty()) {
            throw new NotFoundException("Can't find task by id " + taskId);
        }
        TaskEntity taskEntity = taskOptional.get();

        // получить всех водителей
        List<VehicleDriverEntity> vehicleDrivers = vehicleDriverRepository.findAll();

        // вычислить время, необходимое для подъезда исполнителя задачи до пункта забора пассажиров на основе
        // их расстояние от места выполнения последней задачи до забора пассажиров и времени (otherTime)
        // на загрузку/разгрузку пассажиров
        final int otherTime = 10;
        List<Integer> distances = vehicleDrivers
                .stream()
                .map(e -> calcDistanceBetweenTwoPoints(
                        taskEntity.getRoad().getTargetPoint(),
                        taskEntity.getRoad().getSourcePoint()) / e.getVehicle().getAverageSpeed() + otherTime)
                .collect(Collectors.toList());

        // сформировать dto {requirementCapacity, List[driver_id, distance, capacity]}
        List<String> drivers = vehicleDrivers
                .stream()
                .map(e -> e.getDriver().getId())
                .collect(Collectors.toList());
        List<Integer> capacities = vehicleDrivers
                .stream()
                .map(e -> e.getVehicle().getCapacity())
                .collect(Collectors.toList());
        List<OptimizationVehicleDriverDto> optimizationDtoList = new ArrayList<>();
        for (int i = 0; i < vehicleDrivers.size(); ++i) {
            optimizationDtoList.add(new OptimizationVehicleDriverDto(drivers.get(i), distances.get(i), capacities.get(i)));
        }
        OptimizationRequest optimizationRequest = new OptimizationRequest(taskEntity.getFlight().getPassengerCount(), optimizationDtoList);

        // отправить запрос на приложение оптимизации, получить ответ
        var objectMapper = new ObjectMapper();
        HttpClient client = HttpClient.newHttpClient();
        String http = "http://";
        String host = "127.0.0.1";
        String port = "5000";
        String path = "/";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(http + host +":" + port + path))
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(optimizationRequest)))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        List performerIdRawList = objectMapper.readValue(response.body(), List.class);
        List<String> performerIdList = new ArrayList<>();
        for (Object integer : performerIdRawList) {
            performerIdList.add((String) integer);
        }

        // по полученным id водителей сформировать список entity, назначить его задаче
        List<TaskPerformerEntity> taskPerformerEntities = new ArrayList<>();
        for (var performer: performerIdList) {
            taskPerformerEntities.add(taskPerformerRepository.findById(performer).orElseThrow());
        }
        taskEntity.setTaskPerformers(taskPerformerEntities);

        taskRepository.save(taskEntity);
        return taskPerformerEntities;
    }

    private Integer calcDistanceBetweenTwoPoints(PointEntity pointSourceId, PointEntity pointTargetId) {
        return roadRepository.getDistanceBySourcePointAndTargetPoint(pointSourceId, pointTargetId);
    }

    @Override
    @Transactional
    public void deleteById(String taskId) throws Exception {
        TaskEntity taskEntity = taskRepository.findById(taskId).orElseThrow(()->new NotFoundException("Задача не найдена"));
        taskEntity.setIsDeleted(true);
        taskRepository.save(taskEntity);
    }

    private void dispatch(TaskEntity taskEntity) {
        try {
            List<UserInfoEntity> users = userInfoRepository.findByRoleName("dispatcher");
            List<TaskPerformerEntity> taskPerformerEntities = taskPerformerRepository.findAllByTaskId(taskEntity.getId());

            List<String> toUsers = users.stream().map(UserInfoEntity::getLogin).collect(Collectors.toList());

            if (taskPerformerEntities != null) {
                toUsers.addAll(taskPerformerEntities.stream().map(taskPerformerEntity ->
                                taskPerformerEntity.getVehicleDriver().getDriver().getLogin()).collect(Collectors.toList()));
                eventDispatcher.dispatch(new WsEvent(toUsers, taskMapper.toDto(taskEntity)));

            }
        } catch (Exception e) {
            log.error("Failed to dispatch event", e);
        }
    }

    private boolean canEdit() throws AuthenticationException {
        UserInfoDto currentUser = userService.getCurrentUser();
        return currentUser.getRole().getPriority() >= MIN_PRIORITY_TO_EDIT;
    }
}
