package com.povobolapo.aviahack.service.impl;

import com.povobolapo.aviahack.repository.TaskRepository;
import com.povobolapo.aviahack.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final TaskRepository taskRepository;

    @Override
    public Integer countByTaskPerformersCompleted(String taskPerformerId) {
        return taskRepository.countByTaskPerformersCompleted(taskPerformerId);
    }

    @Override
    public Integer countByTaskPerformersRejected(String taskPerformerId) {
        return taskRepository.countByTaskPerformersRejected(taskPerformerId);
    }

    @Override
    public Integer countByTaskPerformersAccepted(String taskPerformerId) {
        return taskRepository.countByTaskPerformersAccepted(taskPerformerId);
    }


}
