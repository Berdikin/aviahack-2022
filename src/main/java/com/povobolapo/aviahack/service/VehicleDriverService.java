package com.povobolapo.aviahack.service;

import com.povobolapo.aviahack.dto.VehicleDriverDto;

import java.util.List;

public interface VehicleDriverService {
    VehicleDriverDto createVehicleDriver(String login, String vehicleId);
    List<VehicleDriverDto> getAllDrivers();
}
