package com.povobolapo.aviahack.service;


import com.povobolapo.aviahack.dto.TaskPerformerRequest;
import com.povobolapo.aviahack.dto.task.PerformerTaskActionDto;
import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.model.TaskEntity;
import com.povobolapo.aviahack.model.TaskPerformerEntity;

import javax.naming.AuthenticationException;
import java.nio.file.AccessDeniedException;


public interface TaskPerformerService {
    PerformerTaskActionDto getTaskActionByTaskId(String taskId) throws AuthenticationException;
    PerformerTaskActionDto getTaskAction(TaskEntity task) throws AuthenticationException;
    PerformerTaskActionDto getTaskAction(TaskPerformerEntity taskPerformer);
    void changeTaskStatus(PerformerTaskActionDto taskActionDto);
    TaskPerformerDto addTaskPerformer(TaskPerformerRequest performerRequest) throws AuthenticationException, AccessDeniedException;
    void deleteTaskPerformer(TaskPerformerRequest performerRequest) throws AuthenticationException, AccessDeniedException;
}
