package com.povobolapo.aviahack.service;

import com.povobolapo.aviahack.dto.task.TaskDto;
import com.povobolapo.aviahack.dto.task.TaskShortDto;
import com.povobolapo.aviahack.model.TaskPerformerEntity;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.List;

public interface TaskService {

    TaskDto createTask(String flightNumber) throws AccessDeniedException, AuthenticationException;

    void updateTask(TaskDto taskDto) throws AuthenticationException, AccessDeniedException;

    List<TaskShortDto> getTasks() throws AuthenticationException ;

    TaskDto getById(String id);

    List<TaskPerformerEntity> generateTaskPerformers(String taskId) throws IOException, InterruptedException;

    void deleteById(String taskId) throws Exception;

}
