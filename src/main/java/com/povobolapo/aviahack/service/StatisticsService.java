package com.povobolapo.aviahack.service;

public interface StatisticsService {
    Integer countByTaskPerformersCompleted(String taskPerformerId);
    Integer countByTaskPerformersRejected(String taskPerformerId);
    Integer countByTaskPerformersAccepted(String taskPerformerId);

}
