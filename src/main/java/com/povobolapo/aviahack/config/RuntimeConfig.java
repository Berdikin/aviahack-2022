package com.povobolapo.aviahack.config;

import com.povobolapo.aviahack.dispatcher.EventDispatcher;
import com.povobolapo.aviahack.dispatcher.EventDispatcherImpl;
import com.povobolapo.aviahack.dispatcher.EventHandler;
import com.povobolapo.aviahack.websocket.WsSessionManager;
import com.povobolapo.aviahack.websocket.model.WsEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class RuntimeConfig {
    @Bean
    @Scope("singleton")
    public EventDispatcher eventDispatcher(WsSessionManager sessionManager) {
        return new EventDispatcherImpl() {{
            registerHandler(WsEvent.class, (EventHandler<WsEvent>) sessionManager::onEvent);
        }};
    }
}
