package com.povobolapo.aviahack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.povobolapo.aviahack")
public class AviahackApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AviahackApplication.class, args);
	}

}
