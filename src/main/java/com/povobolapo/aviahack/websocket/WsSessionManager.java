package com.povobolapo.aviahack.websocket;

import com.povobolapo.aviahack.websocket.model.WsEvent;

import javax.websocket.Session;

public interface WsSessionManager {
    void addSession(String login, Session session);
    void removeSession(String login);
    void sendObject(String login, Object obj);
    void onEvent(WsEvent event) throws Exception;
}
