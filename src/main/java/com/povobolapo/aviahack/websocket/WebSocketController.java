package com.povobolapo.aviahack.websocket;

import com.povobolapo.aviahack.utils.JwtTokenUtil;
import com.povobolapo.aviahack.websocket.model.MessageDecoder;
import com.povobolapo.aviahack.websocket.model.MessageEncoder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;

import javax.naming.AuthenticationException;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@ServerEndpoint(
        value = "/ws",
        encoders = MessageEncoder.class,
        decoders = MessageDecoder.class)
public class WebSocketController {
    private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);
    private final ConcurrentHashMap<String, Session> activeSessions = new ConcurrentHashMap<>();

    // Через Autowired spring тянуть не хочет
    private final JwtTokenUtil jwtTokenUtil = SpringContext.getBean(JwtTokenUtil.class);
    private final WsSessionManager sessionManager = SpringContext.getBean(WsSessionManager.class);

    @OnOpen
    public void onOpen(Session session) throws AuthenticationException {
        sessionManager.addSession(authUser(session), session);
    }

    @OnMessage
    public void onMessage(Session session, String message) throws IOException, EncodeException {
        session.getBasicRemote().sendObject(message);
    }

    @OnClose
    public void onClose(Session session) throws AuthenticationException {
        sessionManager.removeSession(authUser(session));
    }

    @OnError
    public void onError(Session session, Throwable throwable) throws AuthenticationException {
        sessionManager.removeSession(authUser(session));
    }

    private String authUser(Session session) throws AuthenticationException {
        Map<String, List<String>> params = session.getRequestParameterMap();

        if (params == null) {
            throw new AuthenticationException("Incorrect token");
        }

        if (!params.containsKey("token")) {
            throw new AuthenticationException("Incorrect token");
        }
        List<String> tokenValues = params.get("token");

        if (tokenValues == null || tokenValues.isEmpty()) {
            throw new AuthenticationException("Incorrect token");
        }
        String token = tokenValues.get(0);

        if (StringUtils.isBlank(token)) {
            throw new AuthenticationException("Incorrect token");
        }

        UserDetails userDetails = jwtTokenUtil.validateToken(token);
        log.info("Authenticated user {}", userDetails.getUsername());
        return userDetails.getUsername();
    }
}
