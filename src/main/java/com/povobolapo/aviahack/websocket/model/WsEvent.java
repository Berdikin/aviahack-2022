package com.povobolapo.aviahack.websocket.model;

import com.povobolapo.aviahack.dispatcher.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WsEvent implements Event {
    private List<String> toUsers = new ArrayList<>();
    private Object data;
}
