package com.povobolapo.aviahack.dto.user;

import com.povobolapo.aviahack.dto.RoleDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserInfoDto implements Serializable {
    @Schema(description = "id юзера")
    private String id;
    @NotNull
    @Schema(description = "Логин юзера")
    private String login;
    @NotNull
    @Schema(description = "Роль юзера ну русском")
    private RoleDto role;
    @Schema(description = "ФИО юзера")
    private String name;
    @Schema(description = "Активна ли учетка")
    private boolean active;
}
