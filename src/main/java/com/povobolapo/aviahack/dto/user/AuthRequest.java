package com.povobolapo.aviahack.dto.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class AuthRequest implements Serializable {
    @NotNull
    private String login;
    @NotNull
    private String password;
}
