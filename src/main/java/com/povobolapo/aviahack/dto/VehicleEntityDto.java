package com.povobolapo.aviahack.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleEntityDto implements Serializable {
    private final String id;
    private final String name;
    private final Integer capacity;
    private final Integer averageSpeed;
}
