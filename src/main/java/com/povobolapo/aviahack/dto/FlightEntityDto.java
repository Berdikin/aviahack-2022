package com.povobolapo.aviahack.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class FlightEntityDto implements Serializable {
    private final String id;
    private final LocalDateTime flightDate;
    private final String flightType;
    private final String terminal;
    private final String code;
    private final String flightNumber;
    private final LocalDateTime plannedTime;
    private final String airport;
    private final String planeModel;
    private final LocationEntityDto parkLocation;
    private final LocationEntityDto gateLocation;
    private final Integer passengerCount;
}
