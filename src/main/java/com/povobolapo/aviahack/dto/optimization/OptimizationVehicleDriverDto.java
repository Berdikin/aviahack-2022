package com.povobolapo.aviahack.dto.optimization;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OptimizationVehicleDriverDto {

    @Schema(description = "Идентификатор водителя")
    String driver_id;

    @Schema(description = "Расстояние между транспортным средством и пунктом забора пассажиров")
    Integer distance;

    @Schema(description = "Вместимость транспорта")
    Integer capacity;

}
