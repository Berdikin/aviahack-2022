package com.povobolapo.aviahack.dto.optimization;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Schema(description = "Запрос на нахождение оптимального распределения транспортных средств для перевозки пассажиров")
public class OptimizationRequest {

    @Schema(description = "Количество пассажиров")
    Integer requirementCapacity;

    @Schema(description = "Информация для составления задачи смешанного целочисленного программирования")
    List<OptimizationVehicleDriverDto> vehicleDriver;

}
