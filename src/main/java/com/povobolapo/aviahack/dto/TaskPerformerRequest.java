package com.povobolapo.aviahack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskPerformerRequest {
    @NotNull
    @NotEmpty
    private String taskId;
    @NotNull
    @NotEmpty
    private String driverId;
}
