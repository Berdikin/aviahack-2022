package com.povobolapo.aviahack.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Schema(description = "Информация о транспортном средстве")
public class VehicleDto {

    @Schema(description = "Идентификатор транспортного средства")
    private String id;

    @Schema(description = "Наименование")
    private String name;

    @Schema(description = "Вместимость")
    private Integer capacity;

    @Schema(description = "Средняя скорость")
    private Integer averageSpeed;

}
