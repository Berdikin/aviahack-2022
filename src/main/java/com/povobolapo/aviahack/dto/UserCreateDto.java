package com.povobolapo.aviahack.dto;

import com.povobolapo.aviahack.dto.user.UserInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto extends UserInfoDto {
    @NotNull
    @NotEmpty
    private String password;
}
