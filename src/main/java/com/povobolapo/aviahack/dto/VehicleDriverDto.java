package com.povobolapo.aviahack.dto;

import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.mapper.UserInfoMapper;
import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleDriverDto implements Serializable {
    private final String id;
    private final UserInfoDto driver;
    private final VehicleEntityDto vehicle;
}
