package com.povobolapo.aviahack.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoadEntityDto implements Serializable {
    private final String id;
    private final PointEntityDto sourcePoint;
    private final PointEntityDto targetPoint;
    private final Integer distance;
}
