package com.povobolapo.aviahack.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class LocationEntityDto implements Serializable {
    private final String id;
    private final String locationName;
}
