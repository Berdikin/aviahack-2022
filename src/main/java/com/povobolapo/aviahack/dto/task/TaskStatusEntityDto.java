package com.povobolapo.aviahack.dto.task;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskStatusEntityDto implements Serializable {
    private final String id;
    private final String name;
    private final String caption;
}
