package com.povobolapo.aviahack.dto.task;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerformerTaskActionDto {
    @Schema(description = "Id таски, с которой взаимодействует исполнитель")
    private String taskId;
    @Schema(description = "Id исполнителя")
    private String performerId;
    @Schema(description = "Сигнал дейсвтия", allowableValues = {"accept", "start", "complete"})
    private String signal;
    @Schema(description = "Описание действия")
    private String caption;

    public PerformerTaskActionDto(String signal, String caption) {
        this.signal = signal;
        this.caption = caption;
    }
}
