package com.povobolapo.aviahack.dto.task;

import com.povobolapo.aviahack.dto.VehicleDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Schema(description = "Информация о исполнителе задачи")
public class TaskPerformerDto {
    @Schema(description = "Идентификатор исполнителя задачи")
    private String taskPerformerId;

    @Schema(description = "Состояние задачи")
    private String taskStatus;

    @Schema(description = "ФИО водителя")
    private String driverName;

    @Schema(description = "id водителя")
    private String driverId;

    @Schema(description = "Информация о транспортном средстве")
    private VehicleDto vehicle;


}
