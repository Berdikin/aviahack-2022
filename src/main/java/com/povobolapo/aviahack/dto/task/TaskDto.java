package com.povobolapo.aviahack.dto.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Schema(description = "Информация о задаче")
public class TaskDto {

    @Schema(description = "Идентификатор задачи")
    private String taskId;

    @Schema(description = "Дата создания задачи")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate;

    @Schema(description = "Дата начала выполнения задачи")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)

    private LocalDateTime startDate;

    @Schema(description = "Дата завершения задачи")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime estimationTime;

    @Schema(description = "Название рейса")
    private String flightNumber;

    @Schema(description = "Идентификатор маршрута")
    private String roadId;

    @Schema(description = "Название стартовой точки")
    private String sourcePoint;

    @Schema(description = "Название конечной точки")
    private String targetPoint;

    @Schema(description = "Расстояние в метрах")
    private Integer distance;

    @Schema(description = "Список исполнителей задачи")
    private List<TaskPerformerDto> taskPerformerList;



}
