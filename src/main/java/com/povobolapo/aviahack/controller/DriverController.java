package com.povobolapo.aviahack.controller;

import com.povobolapo.aviahack.dto.VehicleDriverDto;
import com.povobolapo.aviahack.service.VehicleDriverService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/driver")
@Tag(name = "Driver", description = "Работа с водителями")
public class DriverController {
    private final VehicleDriverService vehicleDriverService;

    @Operation(description = "Получение всех водителей")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleDriverDto> getAllDrivers() {
        return vehicleDriverService.getAllDrivers();
    }

}
