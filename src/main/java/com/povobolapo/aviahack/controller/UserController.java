package com.povobolapo.aviahack.controller;

import com.povobolapo.aviahack.dto.user.AuthRequest;
import com.povobolapo.aviahack.dto.user.AuthResponse;
import com.povobolapo.aviahack.dto.user.UserInfoDto;
import com.povobolapo.aviahack.service.UserService;
import com.povobolapo.aviahack.utils.JwtTokenUtil;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.nio.file.AccessDeniedException;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;
    private final UserService userService;

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Авторизация пользователя")
    public AuthResponse createAuthenticationToken(@RequestBody AuthRequest authenticationRequest) {
        log.debug("POST-request: createAuthenticationToken");
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequest.getLogin(), authenticationRequest.getPassword()));
        if (!auth.isAuthenticated()) {
            throw new BadCredentialsException("Wrong user credits");
        }

        String token = jwtTokenUtil.generateToken(authenticationRequest.getLogin());
        return new AuthResponse(token);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Получение юзера по логину")
    public UserInfoDto getUser(@RequestParam String login) {
        log.debug("GET-request: getUser({})", login);
        return userService.getUser(login);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Обновление данных юзера")
    public void updateUser(@RequestBody UserInfoDto userRequest) throws AccessDeniedException, AuthenticationException {
        log.debug("PUT-request: updateUser({})", userRequest);
        userService.updateUser(userRequest);
    }
}
