package com.povobolapo.aviahack.controller;

import com.povobolapo.aviahack.dto.TaskPerformerRequest;
import com.povobolapo.aviahack.dto.task.PerformerTaskActionDto;
import com.povobolapo.aviahack.dto.task.TaskDto;
import com.povobolapo.aviahack.dto.task.TaskPerformerDto;
import com.povobolapo.aviahack.dto.task.TaskShortDto;
import com.povobolapo.aviahack.service.TaskPerformerService;
import com.povobolapo.aviahack.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/task")
@Tag(name = "task", description = "Работа с задачами")
public class TaskController {

    private final TaskService taskService;
    private final TaskPerformerService taskPerformerService;

    @Operation(description = "Получение всех задач")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<TaskShortDto> getAllTasks() throws AuthenticationException {
        return taskService.getTasks();
    }

    @Operation(description = "Получение задачи по id")
    @GetMapping(value = "/{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TaskDto getTaskById(@PathVariable(name = "taskId") String taskId) throws Exception {
        return taskService.getById(taskId);
    }

    @Operation(description = "Создание задачи")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void saveTask(@RequestBody TaskDto request) throws IOException, AuthenticationException {
        // TODO передавать только flightNumber
        taskService.createTask(request.getFlightNumber());
    }

    @Operation(description = "Обновление задачи")
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateTask(@RequestBody TaskDto request) throws IOException, AuthenticationException {
        taskService.updateTask(request);
    }

    @Operation(description = "Удаление задачи по id")
    @DeleteMapping(value = "/{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteTask(@PathVariable(name = "taskId") String taskId) throws Exception {
        taskService.deleteById(taskId);
    }

    @Operation(description = "Получение действий для исполнителя по задаче")
    @GetMapping(value = "/performer/action/{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PerformerTaskActionDto getActions(@PathVariable(name = "taskId") String taskId) throws AuthenticationException {
        return taskPerformerService.getTaskActionByTaskId(taskId);
    }

    @Operation(description = "Действия исполнителя по задаче")
    @PutMapping(value = "/performer/action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void acceptTask(@RequestBody PerformerTaskActionDto performerTaskActionDto) {
        taskPerformerService.changeTaskStatus(performerTaskActionDto);
    }

    @Operation(description = "Добавление водителя исполнителем в таску")
    @PostMapping(value = "/performers/add", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public TaskPerformerDto addPerformer(@RequestBody TaskPerformerRequest performerRequest) throws AccessDeniedException, AuthenticationException {
        return taskPerformerService.addTaskPerformer(performerRequest);
    }

    @Operation(description = "Удаление водителя исполнителем в таску")
    @DeleteMapping(value = "/performers/delete")
    @ResponseStatus(HttpStatus.OK)
    public void deletePerformer(@RequestBody TaskPerformerRequest performerRequest) throws AccessDeniedException, AuthenticationException {
        taskPerformerService.deleteTaskPerformer(performerRequest);
    }
}