package com.povobolapo.aviahack.controller;

import com.povobolapo.aviahack.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RequestMapping(value = "/statistics")
@RequiredArgsConstructor
@RestController
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping(value = "/")
    public ResponseEntity<HashMap<String, Integer>> getStatistics(@RequestParam String taskPerformerId) {
        HashMap<String, Integer> statistics = new HashMap<>();
        statistics.put("completed", statisticsService.countByTaskPerformersCompleted(taskPerformerId));
        statistics.put("rejected", statisticsService.countByTaskPerformersRejected(taskPerformerId));
        statistics.put("accepted", statisticsService.countByTaskPerformersAccepted(taskPerformerId));
        return ResponseEntity.ok(statistics);
    }
    @GetMapping("/count-tasks/accepted")
    public Integer countTasksAccepted(@RequestParam String taskPerformerId) {
      return  statisticsService.countByTaskPerformersAccepted(taskPerformerId);
    }

    @GetMapping("/count-tasks/rejected")
    public Integer countTasksRejected(@RequestParam String taskPerformerId) {
      return   statisticsService.countByTaskPerformersRejected(taskPerformerId);
    }

    @GetMapping("/count-tasks/completed")
    public Integer countTasksCompleted(@RequestParam String taskPerformerId) {
         return statisticsService.countByTaskPerformersCompleted(taskPerformerId);
    }
}
