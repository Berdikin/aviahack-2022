package com.povobolapo.aviahack.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "_tasks")
public class TaskEntity implements Serializable {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id", nullable = false, length = 8)
    private String id;

    @Column(name = "creation_date")
    @JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss.SSSZ")
    private LocalDateTime creationDate;

    @Column(name = "start_date")
    @JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss.SSSZ")
    private LocalDateTime startDate;

    @Column(name = "finish_date")
    private LocalDateTime finishDate;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "flight_id", nullable = false)
    private FlightEntity flight;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "road_id", nullable = false)
    private RoadEntity road;

    @Column(name = "estemation_time")
    private LocalDateTime estimationTime;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToMany(mappedBy = "taskId", fetch = FetchType.EAGER)
    private List<TaskPerformerEntity> taskPerformers = new ArrayList<>();

}