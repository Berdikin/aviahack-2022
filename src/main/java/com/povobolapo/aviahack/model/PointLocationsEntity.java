package com.povobolapo.aviahack.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "_points_locations")
public class PointLocationsEntity {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id", nullable = false, length = 8)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "point_id", nullable = false)
    private PointEntity point;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "location_id", nullable = false)
    private LocationEntity location;
}
