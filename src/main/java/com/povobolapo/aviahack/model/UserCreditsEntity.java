package com.povobolapo.aviahack.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/***
 * WARNING!!!
 * ИСПОЛЬЗОВАТЬ ТОЛЬКО ДЛЯ АВТОРИЗАЦИИ!!!
 * ТОЛЬКО ДЛЯ АВТОРИЗАЦИИ!!!!
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_user_credits")
public class UserCreditsEntity implements Serializable {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id")
    private String id;

    @Column(name = "login", length = 32, nullable = false, unique = true)
    private String login;

    @Column(name = "password", length = 128, nullable = false)
    private String password;

    public UserCreditsEntity(String login, String password) {
        this.login = login;
        this.password = password;
    }

}
