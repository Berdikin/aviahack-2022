package com.povobolapo.aviahack.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "_flights")
public class FlightEntity implements Serializable {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id", nullable = false, length = 8)
    private String id;

    @Column(name = "flight_date")
    private LocalDateTime flightDate;

    @Column(name = "flight_type", length = 1)
    private String flightType;

    @Column(name = "terminal", length = 10)
    private String terminal;

    @Column(name = "code")
    private String code;

    @Column(name = "flight_number")
    private String flightNumber;

    @Column(name = "planned_time")
    private LocalDateTime plannedTime;

    @Column(name = "airport")
    private String airport;

    @Column(name = "plane_model")
    private String planeModel;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "park_location_id", nullable = false)
    private LocationEntity parkLocation;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "gate_location_id", nullable = false)
    private LocationEntity gateLocation;

    @Column(name = "passanger_count")
    private Integer passengerCount;
}