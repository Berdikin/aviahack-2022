package com.povobolapo.aviahack.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "_locations")
public class LocationEntity implements Serializable {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id", nullable = false, length = 8)
    private String id;

    @Column(name = "location_name")
    private String locationName;

}