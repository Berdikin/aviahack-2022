package com.povobolapo.aviahack.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "_task_performers")
@AllArgsConstructor
@NoArgsConstructor
public class TaskPerformerEntity implements Serializable {
    @Id
    @GenericGenerator(name = "entity_id", strategy = "com.povobolapo.aviahack.model.EntityIdGenerator")
    @GeneratedValue(generator = "entity_id")
    @Column(name = "id", nullable = false, length = 8)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vehicle_driver_id", nullable = false)
    private VehicleDriverEntity vehicleDriver;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id")
    private TaskStatusEntity status;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "task_id", nullable = false)
//    private TaskEntity task;

    @Column(name = "task_id")
    private String taskId;
}