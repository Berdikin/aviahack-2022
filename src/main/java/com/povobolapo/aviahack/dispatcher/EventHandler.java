package com.povobolapo.aviahack.dispatcher;

public interface EventHandler<T extends Event>{
    void onEvent(T event) throws Exception;
}
