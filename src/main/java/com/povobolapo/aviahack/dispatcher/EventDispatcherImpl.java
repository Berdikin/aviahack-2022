package com.povobolapo.aviahack.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Класс для вызова определенных методов для определенных классов
 * Для класса регистрируется метод-обработчик через registerHandler
 * После этого можно в любом месте кода вызвать dispatch для класса и выполнится нужны обработчик
 * В данном проекте используется для отправки обновленной задачи по WebSocket
 */
public class EventDispatcherImpl implements EventDispatcher {
    private static final Logger log = LoggerFactory.getLogger(EventDispatcherImpl.class);

    private Map<Class<?>, Set<EventHandler<Event>>> handlers;

    public EventDispatcherImpl() {
        this.handlers = new ConcurrentHashMap<>();
    }

    public void registerHandler(Class<? extends Event> type, EventHandler<? extends Event> handler) {
        handlers.putIfAbsent(type, new CopyOnWriteArraySet<>());
        handlers.get(type).add((EventHandler<Event>) handler);
        log.info("Added new event handler for class {}:: {}", type.getName(), handler.getClass().getName());
    }

    public void dispatch(Event event) throws Exception {
        log.info("Dispatching event {}", event);
        Set<EventHandler<Event>> handler = handlers.get(event.getType());

        if (handler != null) {
            for (EventHandler<Event> h : handler) {
                h.onEvent(event);
            }
        } else {
            log.warn("Not handlers found!");
        }
    }

}
