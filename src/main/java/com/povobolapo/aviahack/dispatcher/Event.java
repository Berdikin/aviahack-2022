package com.povobolapo.aviahack.dispatcher;

public interface Event {
    default Class<?> getType() {
        return getClass();
    }
}
