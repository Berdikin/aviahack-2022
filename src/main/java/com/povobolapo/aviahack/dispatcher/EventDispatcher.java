package com.povobolapo.aviahack.dispatcher;

public interface EventDispatcher {
    void registerHandler(Class<? extends Event> type, EventHandler<? extends Event> handler);
    void dispatch(Event event) throws Exception;
}
