insert into _user_credits values (getnextid(), 'autotest_user', '$2a$10$8vzgsIktNcMSE1/QU49jVeO1dVo2sJFFdHncZbN.QAFEhXovqSJA6');
insert into _user_info (id, creation_date, login, name, active, role_name) values (getnextid(), now(), 'autotest_user', 'Тестовый пользак', true, 'admin');
